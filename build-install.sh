#!/bin/sh

make
doas mkdir -p /usr/local/stow
doas make install
make clean
DIR=$PWD
cd /usr/local/stow
doas stow st
cd $DIR
